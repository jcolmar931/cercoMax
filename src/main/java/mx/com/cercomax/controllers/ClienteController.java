package mx.com.cercomax.controllers;

import mx.com.cercomax.entity.Cliente;
import mx.com.cercomax.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value= "/listar")
    public List<Cliente> getClientes(){

        return clienteService.findAll();
    }
}

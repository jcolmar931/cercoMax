package mx.com.cercomax.dao;

import mx.com.cercomax.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteDao extends CrudRepository<Cliente,Long> {

}

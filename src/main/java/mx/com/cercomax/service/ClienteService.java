package mx.com.cercomax.service;

import mx.com.cercomax.dao.ClienteDao;
import mx.com.cercomax.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ClienteService {

    public List<Cliente> findAll();
}

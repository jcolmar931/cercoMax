package mx.com.cercomax.service;

import mx.com.cercomax.dao.ClienteDao;
import mx.com.cercomax.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteServiceImpl implements ClienteService{

    @Autowired
    private ClienteDao clienteDao;

    @Transactional(readOnly = true)
    @Override
    public List<Cliente> findAll() {

        return (List<Cliente>) clienteDao.findAll();
    }
}

package mx.com.cercomax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CercomaxApplication {

    public static void main(String[] args) {
        SpringApplication.run(CercomaxApplication.class, args);
    }

}
